use std::borrow::Cow;

use qdb_api_sys::{ClusterHandle, Connected};

pub mod crypto;

pub type Cluster = ClusterHandle<Connected>;
pub use qdb_api_sys::ApiError;

pub fn insecure_connect(cluster_url: Cow<'_, str>) -> Result<Cluster, ApiError> {
    let cluster = ClusterHandle::new()?;
    cluster.insecure_connect(&cluster_url)
}
