use std::{ffi::CString, time::SystemTime};

use log::info;
use qdb_api_sys::{
    self, ApiError, ClusterHandle, Connected, ContinuousQuery, TsBatchColumnInfo, TsColumnData,
    TsColumnInfo,
};
use time::{format_description::well_known::Rfc3339, Duration, OffsetDateTime};

use crate::Cluster;

/// The data structure to fill in order to use the 'coinbase' engine for orderbooks
///
/// The engine only works on L3
pub struct CoinbaseEngineOrderbookRow {
    pub timestamp: SystemTime,
    pub type_: String,
    pub order_id: String,
    pub sequence: i64,
    pub reason: String,
    pub stop: String,
    pub price: f64,
    pub funds: f64,
    pub size: f64,
    pub old_size: f64,
}

impl CoinbaseEngineOrderbookRow {
    pub fn batch_columns(table_name: String, size_hint: usize) -> Vec<TsBatchColumnInfo> {
        vec![
            TsBatchColumnInfo::string(&table_name, "type", size_hint).unwrap(),
            TsBatchColumnInfo::string(&table_name, "order_id", size_hint).unwrap(),
            TsBatchColumnInfo::int64(&table_name, "sequence", size_hint).unwrap(),
            TsBatchColumnInfo::string(&table_name, "reason", size_hint).unwrap(),
            TsBatchColumnInfo::string(&table_name, "stop", size_hint).unwrap(),
            TsBatchColumnInfo::double(&table_name, "price", size_hint).unwrap(),
            TsBatchColumnInfo::double(&table_name, "funds", size_hint).unwrap(),
            TsBatchColumnInfo::double(&table_name, "size", size_hint).unwrap(),
            TsBatchColumnInfo::double(&table_name, "old_size", size_hint).unwrap(),
        ]
    }

    pub fn table_columns() -> Vec<TsColumnInfo> {
        vec![
            TsColumnInfo::string("type").unwrap(),
            TsColumnInfo::string("order_id").unwrap(),
            TsColumnInfo::int64("sequence").unwrap(),
            TsColumnInfo::string("reason").unwrap(),
            TsColumnInfo::string("stop").unwrap(),
            TsColumnInfo::double("price").unwrap(),
            TsColumnInfo::double("funds").unwrap(),
            TsColumnInfo::double("size").unwrap(),
            TsColumnInfo::double("old_size").unwrap(),
        ]
    }
}

/// The order of columns will match the one from
/// [batch_columns](CoinbaseEngineOrderbookRow::batch_columns).
impl From<CoinbaseEngineOrderbookRow> for Vec<TsColumnData> {
    fn from(row: CoinbaseEngineOrderbookRow) -> Self {
        vec![
            TsColumnData::String(CString::new(row.type_).unwrap()),
            TsColumnData::String(CString::new(row.order_id).unwrap()),
            TsColumnData::Int64(row.sequence),
            TsColumnData::String(CString::new(row.reason).unwrap()),
            TsColumnData::String(CString::new(row.stop).unwrap()),
            TsColumnData::Double(row.price),
            TsColumnData::Double(row.funds),
            TsColumnData::Double(row.size),
            TsColumnData::Double(row.old_size),
        ]
    }
}

/// The data structure to fill in order to use batch inserts for trades
pub struct TradeRow {
    pub timestamp: SystemTime,
    pub trade_id: String,
    pub maker_order_id: String,
    pub taker_order_id: String,
    pub sequence: i64,
    // Aggressor / taker side
    pub side: String,
    pub price: f64,
    pub size: f64,
}

impl TradeRow {
    pub fn batch_columns(table_name: String, size_hint: usize) -> Vec<TsBatchColumnInfo> {
        vec![
            TsBatchColumnInfo::string(&table_name, "trade_id", size_hint).unwrap(),
            TsBatchColumnInfo::string(&table_name, "maker_order_id", size_hint).unwrap(),
            TsBatchColumnInfo::string(&table_name, "taker_order_id", size_hint).unwrap(),
            TsBatchColumnInfo::int64(&table_name, "sequence", size_hint).unwrap(),
            TsBatchColumnInfo::string(&table_name, "side", size_hint).unwrap(),
            TsBatchColumnInfo::double(&table_name, "price", size_hint).unwrap(),
            TsBatchColumnInfo::double(&table_name, "size", size_hint).unwrap(),
        ]
    }

    pub fn table_columns() -> Vec<TsColumnInfo> {
        vec![
            TsColumnInfo::string("trade_id").unwrap(),
            TsColumnInfo::string("maker_order_id").unwrap(),
            TsColumnInfo::string("taker_order_id").unwrap(),
            TsColumnInfo::int64("sequence").unwrap(),
            TsColumnInfo::string("side").unwrap(),
            TsColumnInfo::double("price").unwrap(),
            TsColumnInfo::double("size").unwrap(),
        ]
    }
}

/// The order of columns will match the one from
/// [batch_columns](TradeRow::batch_columns).
impl From<TradeRow> for Vec<TsColumnData> {
    fn from(row: TradeRow) -> Self {
        vec![
            TsColumnData::String(CString::new(row.trade_id).unwrap()),
            TsColumnData::String(CString::new(row.maker_order_id).unwrap()),
            TsColumnData::String(CString::new(row.taker_order_id).unwrap()),
            TsColumnData::Int64(row.sequence),
            TsColumnData::String(CString::new(row.side).unwrap()),
            TsColumnData::Double(row.price),
            TsColumnData::Double(row.size),
        ]
    }
}

pub fn push_bid_batch(
    cluster: &ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
    data: Vec<CoinbaseEngineOrderbookRow>,
) -> Result<(), ApiError> {
    let table_name = bid_table_name(venue, instrument);
    let inserter = cluster.batch_inserter(CoinbaseEngineOrderbookRow::batch_columns(
        table_name,
        data.len(),
    ))?;
    let bid_rows: Vec<(SystemTime, Vec<TsColumnData>)> = data
        .into_iter()
        .map(|bid| {
            let ts = bid.timestamp;
            let bid_row: Vec<TsColumnData> = bid.into();
            (ts, bid_row)
        })
        .collect();
    let ffi_rows = bid_rows
        .iter()
        .map(|(ts, row)| (*ts, row.as_slice()))
        .collect::<Vec<_>>();
    inserter.push_rows(&ffi_rows[..])
}

pub fn push_ask_batch(
    cluster: &ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
    data: Vec<CoinbaseEngineOrderbookRow>,
) -> Result<(), ApiError> {
    let table_name = ask_table_name(venue, instrument);
    let inserter = cluster.batch_inserter(CoinbaseEngineOrderbookRow::batch_columns(
        table_name,
        data.len(),
    ))?;
    let ask_rows: Vec<(SystemTime, Vec<TsColumnData>)> = data
        .into_iter()
        .map(|ask| {
            let ts = ask.timestamp;
            let ask_row: Vec<TsColumnData> = ask.into();
            (ts, ask_row)
        })
        .collect();
    let ffi_rows = ask_rows
        .iter()
        .map(|(ts, row)| (*ts, row.as_slice()))
        .collect::<Vec<_>>();
    inserter.push_rows(&ffi_rows[..])
}

pub fn push_trade(
    cluster: &Cluster,
    venue: &str,
    instrument: &str,
    trade: TradeRow,
) -> Result<(), ApiError> {
    let table_name = trade_table_name(venue, instrument);
    let inserter = cluster.batch_inserter(TradeRow::batch_columns(table_name, 1))?;
    let ts = trade.timestamp;
    let trade_row: Vec<TsColumnData> = trade.into();
    let trade_rows = vec![(ts, &trade_row[..])];
    inserter.push_rows(&trade_rows[..])
}

pub fn push_trade_batch(
    cluster: &ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
    data: Vec<TradeRow>,
) -> Result<(), ApiError> {
    let table_name = trade_table_name(venue, instrument);
    let inserter = cluster.batch_inserter(TradeRow::batch_columns(table_name, data.len()))?;
    let trade_rows: Vec<(SystemTime, Vec<TsColumnData>)> = data
        .into_iter()
        .map(|trade| {
            let ts = trade.timestamp;
            let trade_row: Vec<TsColumnData> = trade.into();
            (ts, trade_row)
        })
        .collect();
    let ffi_rows = trade_rows
        .iter()
        .map(|(ts, row)| (*ts, row.as_slice()))
        .collect::<Vec<_>>();
    inserter.push_rows(&ffi_rows[..])
}

pub fn continuous_trades<'handle>(
    _cluster: &'handle ClusterHandle<Connected>,
    _venue: &str,
    _instrument: &str,
    _since: SystemTime,
) -> Result<ContinuousQuery<'handle>, ApiError> {
    todo!()
}

pub fn continuous_l2_book<'handle>(
    cluster: &'handle ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
    granularity: std::time::Duration,
    max_levels: Option<usize>,
) -> Result<ContinuousQuery<'handle>, ApiError> {
    let data_points = 200;
    let start_date = OffsetDateTime::now_utc()
        .saturating_sub(Duration::seconds(
            (data_points * granularity.as_secs()).try_into().unwrap(),
        ))
        .format(&Rfc3339)
        .unwrap();
    let query = format!(
        "ORDERBOOK ((SELECT * FROM \"{}\", \"{}\" ORDER BY sequence), ({}, now, {}s), engine=coinbase, product='{}', mode=collapsed{})",
        bid_table_name(venue, instrument),
        ask_table_name(venue, instrument),
        start_date,
        granularity.as_secs(),
        instrument,
        if let Some(levels) = max_levels {
            format!(", max_level={}", levels)
        } else {
            String::default()
        }
    );
    info!("Starting continuous query on '{}'", &query);
    cluster.query_continuous(query)
}

pub fn create_trades_table(
    cluster: &ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
) -> Result<(), ApiError> {
    cluster.ts_create(
        &trade_table_name(venue, instrument),
        60 * 60 * 1_000,
        &TradeRow::table_columns()[..],
    )
}

pub fn create_orderbook_ask_table(
    cluster: &ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
) -> Result<(), ApiError> {
    cluster.ts_create(
        &ask_table_name(venue, instrument),
        15 * 60 * 1_000,
        &CoinbaseEngineOrderbookRow::table_columns()[..],
    )
}

pub fn create_orderbook_bid_table(
    cluster: &ClusterHandle<Connected>,
    venue: &str,
    instrument: &str,
) -> Result<(), ApiError> {
    cluster.ts_create(
        &bid_table_name(venue, instrument),
        15 * 60 * 1_000,
        &CoinbaseEngineOrderbookRow::table_columns()[..],
    )
}

fn trade_table_name(venue: &str, instrument: &str) -> String {
    format!("{}/{}/trades", venue, instrument)
}

fn bid_table_name(venue: &str, instrument: &str) -> String {
    format!("{}/{}/orders_buy", venue, instrument)
}

fn ask_table_name(venue: &str, instrument: &str) -> String {
    format!("{}/{}/orders_sell", venue, instrument)
}
