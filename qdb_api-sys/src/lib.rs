use core::fmt;
use std::{
    ffi::{CStr, CString},
    fmt::Display,
    marker::PhantomData,
    mem::MaybeUninit,
    ops::Range,
    path::PathBuf,
    pin::Pin,
    sync::{
        mpsc::{channel, Receiver, Sender},
        Arc,
    },
    time::{Duration, SystemTime, UNIX_EPOCH},
};

use log::trace;

mod bindings;
pub type FfiTimespec = bindings::qdb_timespec_t;

#[derive(Debug, Clone)]
pub struct ApiError(String);

impl From<String> for ApiError {
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for ApiError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

/// Wrapper around the error codes sent by multiple FFI functions
pub struct Status(bindings::qdb_status_t);

impl From<Status> for Result<(), ApiError> {
    fn from(status: Status) -> Self {
        if status.0 == bindings::qdb_error_t_qdb_e_ok {
            Ok(())
        } else {
            // Safety:
            // QuasarDB API certifies the returned value
            // of qdb_error points to a static string
            Err(ApiError(unsafe {
                CStr::from_ptr(bindings::qdb_error(status.0))
                    .to_string_lossy()
                    .into_owned()
            }))
        }
    }
}

mod private {
    /// Sealed trait for connection status
    pub trait SealedConnectionState {}
    impl SealedConnectionState for super::NotConnected {}
    impl SealedConnectionState for super::Connected {}
}
pub trait ConnectionState: private::SealedConnectionState + 'static {}
pub struct NotConnected {}
impl ConnectionState for NotConnected {}
pub struct Connected {}
impl ConnectionState for Connected {}

pub struct ClusterHandle<T: ConnectionState> {
    raw_handle: bindings::qdb_handle_t,
    connection_state: PhantomData<T>,
}

// SAFETY
// The structure only contains a pointer and it is safe to 
// share a reference to that pointer once the connection has been created
unsafe impl<T: ConnectionState> Send for ClusterHandle<T> {}

impl ClusterHandle<NotConnected> {
    /// Failible constructor
    pub fn new() -> Result<Self, ApiError> {
        let mut uninit_handle = MaybeUninit::<bindings::qdb_handle_t>::uninit();
        let status: Result<(), ApiError> = Status(unsafe {
            bindings::qdb_open(
                uninit_handle.as_mut_ptr(),
                bindings::qdb_protocol_t_qdb_p_tcp,
            )
        })
        .into();
        let init_handle = unsafe { uninit_handle.assume_init() };
        status.map(|_| ClusterHandle {
            raw_handle: init_handle,
            connection_state: PhantomData,
        })
    }

    /// Make an insecure connection to a cluster
    pub fn insecure_connect(self, url: &str) -> Result<ClusterHandle<Connected>, ApiError> {
        let c_url = CString::new(url)
            .map_err(|err| format!("Could not convert URL to C string: {}", err))?;
        let status: Result<(), ApiError> =
            Status(unsafe { bindings::qdb_connect(self.raw_handle, c_url.as_ptr()) }).into();
        status.map(|_| ClusterHandle {
            raw_handle: self.raw_handle,
            connection_state: PhantomData,
        })
    }

    /// Make a secure connection to a cluster
    pub fn secure_connect(
        self,
        url: &str,
        options: SecureConnectionOptions,
    ) -> Result<ClusterHandle<Connected>, ApiError> {
        let c_url = CString::new(url)
            .map_err(|err| format!("Could not convert URL to C string: {}", err))?;
        let status: Result<(), ApiError> = match options {
            SecureConnectionOptions::InMemory {
                cluster_public_key,
                username,
                user_private_key,
            } => {
                let c_cluster_key = CString::new(cluster_public_key).map_err(|err| {
                    ApiError(format!(
                        "Could not convert cluster public key to C string: {}",
                        err
                    ))
                })?;
                let c_username = CString::new(username).map_err(|err| {
                    ApiError(format!("Could not convert username to C string: {}", err))
                })?;
                let c_user_key = CString::new(user_private_key).map_err(|err| {
                    ApiError(format!(
                        "Could not convert user private key to C string: {}",
                        err
                    ))
                })?;
                Result::<(), ApiError>::from(Status(unsafe {
                    bindings::qdb_option_set_cluster_public_key(
                        self.raw_handle,
                        c_cluster_key.as_ptr(),
                    )
                }))
                .map_err(|err| ApiError(format!("Could not set cluster public key: {}", err)))
                .and_then(|_| {
                    Result::<(), ApiError>::from(Status(unsafe {
                        bindings::qdb_option_set_user_credentials(
                            self.raw_handle,
                            c_username.as_ptr(),
                            c_user_key.as_ptr(),
                        )
                    }))
                })
                .map_err(|err| ApiError(format!("Could not set user credentials: {}", err)))
                .and_then(|_| {
                    Status(unsafe { bindings::qdb_connect(self.raw_handle, c_url.as_ptr()) }).into()
                })
            }
            SecureConnectionOptions::SecurityFiles {
                cluster_public_key,
                user_credentials,
            } => {
                let c_cluster_path = cluster_public_key
                    .as_os_str()
                    .to_str()
                    .ok_or_else(|| {
                        ApiError("cluster_public_key path is not valid UTF-8".to_string())
                    })
                    .and_then(|path| {
                        CString::new(path).map_err(|err| {
                            ApiError(format!(
                                "Could not convert cluster public key path to C string: {}",
                                err
                            ))
                        })
                    })?;
                let c_cred_path = user_credentials
                    .as_os_str()
                    .to_str()
                    .ok_or_else(|| ApiError("user_credentials path is not valid UTF-8".to_string()))
                    .and_then(|path| {
                        CString::new(path).map_err(|err| {
                            ApiError(format!(
                                "Could not convert credentials path to C string: {}",
                                err
                            ))
                        })
                    })?;
                Result::<(), ApiError>::from(Status(unsafe {
                    bindings::qdb_option_load_security_files(
                        self.raw_handle,
                        c_cluster_path.as_ptr(),
                        c_cred_path.as_ptr(),
                    )
                }))
                .map_err(|err| ApiError(format!("Could not load security files: {}", err)))
                .and_then(|_| {
                    Status(unsafe { bindings::qdb_connect(self.raw_handle, c_url.as_ptr()) }).into()
                })
            }
        };
        status.map(|_| ClusterHandle {
            raw_handle: self.raw_handle,
            connection_state: PhantomData,
        })
    }
}

impl ClusterHandle<Connected> {
    /// Safe qdb_ts_create wrapper
    pub fn ts_create(
        &self,
        name: &str,
        shard_size_ms: u64,
        columns: &[TsColumnInfo],
    ) -> Result<(), ApiError> {
        let c_name = CString::new(name)
            .map_err(|err| format!("Could not convert table name to C string: {}", err))?;
        let col_infos: Vec<_> = columns
            .iter()
            .map(|info| {
                Ok(bindings::qdb_ts_column_info_t {
                    name: info.name.as_ptr(),
                    type_: info.col_type.into(),
                })
            })
            .collect::<Result<Vec<_>, ApiError>>()?;

        Status(unsafe {
            bindings::qdb_ts_create(
                self.raw_handle,
                c_name.as_ptr(),
                shard_size_ms,
                col_infos.as_slice().as_ptr(),
                col_infos.len().try_into().map_err(|_| {
                    "Too many columns in the table, it must fit in a usize".to_string()
                })?,
            )
        })
        .into()
    }

    /// Safe qdb_attach_tag wrapper
    pub fn attach_tag(&self, table_name: &str, tag: &str) -> Result<(), ApiError> {
        let c_table_name = CString::new(table_name)
            .map_err(|err| format!("Could not convert table name to C string: {}", err))?;
        let c_tag = CString::new(tag)
            .map_err(|err| format!("Could not convert tag to C string: {}", err))?;

        Status(unsafe {
            bindings::qdb_attach_tag(self.raw_handle, c_table_name.as_ptr(), c_tag.as_ptr())
        })
        .into()
    }

    /// Safe qdb_batch_table_init wrapper
    pub fn batch_inserter(
        &self,
        columns: Vec<TsBatchColumnInfo>,
    ) -> Result<BatchInserter<'_>, ApiError> {
        let mut uninit_inserter = MaybeUninit::<bindings::qdb_batch_table_t>::uninit();

        let col_infos: Vec<_> = columns
            .iter()
            .map(|info| {
                Ok(bindings::qdb_ts_batch_column_info_t {
                    timeseries: info.timeseries.as_ptr(),
                    column: info.column.as_ptr(),
                    elements_count_hint: info
                        .elements_count_hint
                        .try_into()
                        .map_err(|_| "Too many hints, it must fit in a usize".to_string())?,
                })
            })
            .collect::<Result<Vec<_>, ApiError>>()?;

        let status: Result<(), ApiError> = Status(unsafe {
            bindings::qdb_ts_batch_table_init(
                self.raw_handle,
                col_infos.as_slice().as_ptr(),
                col_infos.len().try_into().map_err(|_| {
                    "Too many columns in the table, it must fit in a usize".to_string()
                })?,
                uninit_inserter.as_mut_ptr(),
            )
        })
        .into();
        let raw_inserter = unsafe { uninit_inserter.assume_init() };
        status.map(|_| BatchInserter {
            parent_handle: self,
            raw_inserter,
            columns,
        })
    }

    /// Safe qdb_query_continuous wrapper
    pub fn query_continuous(&self, query: String) -> Result<ContinuousQuery<'_>, ApiError> {
        let c_query =
            Pin::new(CString::new(query.clone()).map_err(|err| {
                format!("Could not convert query {:?} to C string: {}", query, err)
            })?);

        let (sender, receiver) = channel();
        // Putting the sender in an Arc means that when using Arc::into_raw we
        // are preserving the strong reference count to prevent the receiver from
        // thinking that the channel has been closed.
        let sender = Arc::new(sender);

        let mut raw_cont_query: bindings::qdb_query_cont_handle_t = std::ptr::null_mut();
        let status: Result<(), ApiError> = Status(unsafe {
            // log::info!("Call arguments:\n\tCluster Handle: {:?}\n\tQuery: {:?}\n\tMode: {}\n\tOut param: {:?}",
            // self.raw_handle,
            // c_query,
            // bindings::qdb_query_continuous_mode_type_t_qdb_query_continuous_new_values_only,
            // raw_cont_query);
            bindings::qdb_query_continuous(
                self.raw_handle,
                c_query.as_ptr(),
                bindings::qdb_query_continuous_mode_type_t_qdb_query_continuous_new_values_only,
                Some(cont_query_callback),
                Arc::downgrade(&sender).into_raw() as *mut std::ffi::c_void,
                // None,
                // std::ptr::null_mut(),
                &mut raw_cont_query as *mut bindings::qdb_query_cont_handle_t,
            )
        })
        .into();

        status.map(|_| ContinuousQuery {
            raw_handle: raw_cont_query,
            parent_handle: self,
            query: c_query,
            results: receiver,
        })
    }
}

impl<T: ConnectionState> Drop for ClusterHandle<T> {
    fn drop(&mut self) {
        // Closing the connection on a NotConnected handle is bad
        // (because the connect methods consume a ClusterHandle<NotConnected>, calling the Drop on the same raw_handle pointer)
        // but Rust doesn't allow specialized Drops. So we need to dynamically check the typed connection_state here.
        if std::any::TypeId::of::<T>() == std::any::TypeId::of::<Connected>() {
            unsafe { bindings::qdb_close(self.raw_handle) };
        }
    }
}

/// Parameters for Secure connection
pub enum SecureConnectionOptions {
    InMemory {
        cluster_public_key: String,
        username: String,
        user_private_key: String,
    },
    SecurityFiles {
        cluster_public_key: PathBuf,
        user_credentials: PathBuf,
    },
}

/// Timeseries column data types
#[derive(Debug, Clone, Copy)]
pub enum TsColumnType {
    Uninitialized,
    Double,
    Blob,
    Int64,
    Timestamp,
    String,
    Unknown,
}

impl From<bindings::qdb_ts_column_type_t> for TsColumnType {
    fn from(val: bindings::qdb_ts_column_type_t) -> Self {
        if val == bindings::qdb_ts_column_type_t_qdb_ts_column_uninitialized {
            return Self::Uninitialized;
        }
        if val == bindings::qdb_ts_column_type_t_qdb_ts_column_double {
            return Self::Double;
        }
        if val == bindings::qdb_ts_column_type_t_qdb_ts_column_blob {
            return Self::Blob;
        }
        if val == bindings::qdb_ts_column_type_t_qdb_ts_column_int64 {
            return Self::Int64;
        }
        if val == bindings::qdb_ts_column_type_t_qdb_ts_column_timestamp {
            return Self::Timestamp;
        }
        if val == bindings::qdb_ts_column_type_t_qdb_ts_column_string {
            return Self::String;
        }
        Self::Unknown
    }
}

impl From<TsColumnType> for bindings::qdb_ts_column_type_t {
    fn from(val: TsColumnType) -> Self {
        match val {
            TsColumnType::Uninitialized => unreachable!("Do not build uninitialized columns"),
            TsColumnType::Double => bindings::qdb_ts_column_type_t_qdb_ts_column_double,
            TsColumnType::Blob => bindings::qdb_ts_column_type_t_qdb_ts_column_blob,
            TsColumnType::Int64 => bindings::qdb_ts_column_type_t_qdb_ts_column_int64,
            TsColumnType::Timestamp => bindings::qdb_ts_column_type_t_qdb_ts_column_timestamp,
            TsColumnType::String => bindings::qdb_ts_column_type_t_qdb_ts_column_string,
            TsColumnType::Unknown => unreachable!("Do not build unknown columns"),
        }
    }
}

pub struct TsColumnInfo {
    pub name: CString,
    pub col_type: TsColumnType,
}

impl TsColumnInfo {
    fn try_new(name: String, col_type: TsColumnType) -> Result<Self, ApiError> {
        Ok(Self {
            name: CString::new(name.clone()).map_err(|err| {
                ApiError(format!(
                    "Could not convert column name {} to C string: {}",
                    name, err
                ))
            })?,
            col_type,
        })
    }
    pub fn double<T: ToString>(name: T) -> Result<Self, ApiError> {
        Self::try_new(name.to_string(), TsColumnType::Double)
    }
    pub fn blob<T: ToString>(name: T) -> Result<Self, ApiError> {
        Self::try_new(name.to_string(), TsColumnType::Blob)
    }
    pub fn int64<T: ToString>(name: T) -> Result<Self, ApiError> {
        Self::try_new(name.to_string(), TsColumnType::Int64)
    }
    pub fn timestamp<T: ToString>(name: T) -> Result<Self, ApiError> {
        Self::try_new(name.to_string(), TsColumnType::Timestamp)
    }
    pub fn string<T: ToString>(name: T) -> Result<Self, ApiError> {
        Self::try_new(name.to_string(), TsColumnType::String)
    }
}

pub struct TsBatchColumnInfo {
    pub timeseries: CString,
    pub column: CString,
    pub elements_count_hint: usize,
    pub col_type: TsColumnType,
}

impl TsBatchColumnInfo {
    fn try_new(
        timeseries: String,
        col_name: String,
        elements_count_hint: usize,
        col_type: TsColumnType,
    ) -> Result<Self, ApiError> {
        Ok(Self {
            timeseries: CString::new(timeseries.clone()).map_err(|err| {
                ApiError(format!(
                    "Could not convert column name {} to C string: {}",
                    timeseries, err
                ))
            })?,
            column: CString::new(col_name.clone()).map_err(|err| {
                ApiError(format!(
                    "Could not convert column name {} to C string: {}",
                    col_name, err
                ))
            })?,
            elements_count_hint,
            col_type,
        })
    }
    pub fn double<T: ToString, U: ToString>(
        timeseries: U,
        col_name: T,
        hint: usize,
    ) -> Result<Self, ApiError> {
        Self::try_new(
            timeseries.to_string(),
            col_name.to_string(),
            hint,
            TsColumnType::Double,
        )
    }
    pub fn blob<T: ToString, U: ToString>(
        timeseries: U,
        col_name: T,
        hint: usize,
    ) -> Result<Self, ApiError> {
        Self::try_new(
            timeseries.to_string(),
            col_name.to_string(),
            hint,
            TsColumnType::Blob,
        )
    }
    pub fn int64<T: ToString, U: ToString>(
        timeseries: U,
        col_name: T,
        hint: usize,
    ) -> Result<Self, ApiError> {
        Self::try_new(
            timeseries.to_string(),
            col_name.to_string(),
            hint,
            TsColumnType::Int64,
        )
    }
    pub fn timestamp<T: ToString, U: ToString>(
        timeseries: U,
        col_name: T,
        hint: usize,
    ) -> Result<Self, ApiError> {
        Self::try_new(
            timeseries.to_string(),
            col_name.to_string(),
            hint,
            TsColumnType::Timestamp,
        )
    }
    pub fn string<T: ToString, U: ToString>(
        timeseries: U,
        col_name: T,
        hint: usize,
    ) -> Result<Self, ApiError> {
        Self::try_new(
            timeseries.to_string(),
            col_name.to_string(),
            hint,
            TsColumnType::String,
        )
    }
}

/// Timeseries column data
#[derive(Debug, Clone)]
pub enum TsColumnData {
    Double(f64),
    Blob(Vec<u8>),
    Int64(i64),
    Timestamp(FfiTimespec),
    String(CString),
}

pub struct BatchInserter<'handle> {
    raw_inserter: bindings::qdb_batch_table_t,
    parent_handle: &'handle ClusterHandle<Connected>,
    columns: Vec<TsBatchColumnInfo>,
}

impl<'handle> BatchInserter<'handle> {
    /// Safe wrapper around qdb_ts_batch_start_row and qdb_ts_batch_row_set_* and qdb_ts_batch_push
    pub fn push_rows(&self, rows: &[(SystemTime, &[TsColumnData])]) -> Result<(), ApiError> {
        rows.iter()
            .try_for_each(|(ts, row)| self.add_row(*ts, row))?;
        self.finalize()
    }

    /// Safe wrapper around qdb_ts_batch_start_row and qdb_ts_batch_row_set_*
    pub fn add_row(&self, ts: SystemTime, row: &[TsColumnData]) -> Result<(), ApiError> {
        // Check that all types are correct before starting the row.
        if row.len() != self.columns.len() {
            return Err(format!(
                "Mismatch in row length: expected {}, got {}.",
                self.columns.len(),
                row.len()
            )
            .into());
        }
        row.iter()
            .zip(self.columns.iter())
            .enumerate()
            .try_for_each(|(i, (item, expected))| match (item, expected.col_type) {
                (TsColumnData::Double(_), TsColumnType::Double)
                | (TsColumnData::Blob(_), TsColumnType::Blob)
                | (TsColumnData::Int64(_), TsColumnType::Int64)
                | (TsColumnData::Timestamp(_), TsColumnType::Timestamp)
                | (TsColumnData::String(_), TsColumnType::String) => Ok(()),
                _ => Err(format!("Type mismatch on index {}", i)),
            })?;

        // Check that the timestamp is fine
        let c_ts: bindings::qdb_timespec_t = ts.try_into()?;

        // Now we are sure that we can push a row in the batch inserter
        // TODO: Stop panicking
        unsafe {
            bindings::qdb_ts_batch_start_row(self.raw_inserter, &c_ts);
            for (i, item) in row.iter().enumerate() {
                match item {
                    TsColumnData::Double(val) => {
                        trace!("Index {}: Double {}", i, val);
                        bindings::qdb_ts_batch_row_set_double(
                            self.raw_inserter,
                            i.try_into().unwrap(),
                            *val,
                        );
                    }
                    TsColumnData::Blob(val) => {
                        trace!("Index {}: Blob {:?}", i, val);
                        bindings::qdb_ts_batch_row_set_blob_no_copy(
                            self.raw_inserter,
                            i.try_into().unwrap(),
                            val.as_slice().as_ptr() as *const std::ffi::c_void,
                            val.len().try_into().unwrap(),
                        );
                    }
                    TsColumnData::Int64(val) => {
                        trace!("Index {}: Int64 {}", i, val);
                        bindings::qdb_ts_batch_row_set_int64(
                            self.raw_inserter,
                            i.try_into().unwrap(),
                            *val,
                        );
                    }
                    TsColumnData::Timestamp(val) => {
                        trace!("Index {}: SystemTime {:?}", i, val);
                        bindings::qdb_ts_batch_row_set_timestamp(
                            self.raw_inserter,
                            i.try_into().unwrap(),
                            val,
                        );
                    }
                    TsColumnData::String(val) => {
                        trace!("Index {}: String {:?}", i, val);
                        let len = val.as_bytes().len();
                        bindings::qdb_ts_batch_row_set_string_no_copy(
                            self.raw_inserter,
                            i.try_into().unwrap(),
                            val.as_ptr(),
                            len.try_into().unwrap(),
                        );
                    }
                }
            }
        }
        Ok(())
    }

    /// Safe wrapper around qdb_ts_batch_push
    fn finalize(&self) -> Result<(), ApiError> {
        Status(unsafe { bindings::qdb_ts_batch_push(self.raw_inserter) }).into()
    }
}

impl<'handle> Drop for BatchInserter<'handle> {
    fn drop(&mut self) {
        // Safety: The 'handle lifetime on a _Connected_ QdbHandle ensures that
        // the pointer to the handle is okay to use
        unsafe {
            bindings::qdb_release(
                self.parent_handle.raw_handle,
                self.raw_inserter as *const std::ffi::c_void,
            )
        }
    }
}

impl TryFrom<SystemTime> for bindings::qdb_timespec_t {
    type Error = String;

    fn try_from(inst: SystemTime) -> Result<Self, Self::Error> {
        let duration = inst
            .duration_since(UNIX_EPOCH)
            .map_err(|_| "Negative timestamps cannot be passed to quasar".to_string())?;
        Ok(Self {
            tv_sec: duration
                .as_secs()
                .try_into()
                .map_err(|_| "Too many seconds to be properly converted.".to_string())?,
            tv_nsec: duration.subsec_nanos().into(),
        })
    }
}

impl TryFrom<bindings::qdb_timespec_t> for SystemTime {
    type Error = String;

    fn try_from(inst: bindings::qdb_timespec_t) -> Result<Self, Self::Error> {
        Self::UNIX_EPOCH
            .checked_add(
                Duration::from_secs(
                    inst.tv_sec
                        .try_into()
                        .map_err(|_| "Timestamp too big".to_string())?,
                )
                .checked_add(Duration::from_nanos(
                    inst.tv_nsec
                        .try_into()
                        .map_err(|_| "Timestamp too big".to_string())?,
                ))
                .ok_or_else(|| "Timestamp overflow".to_string())?,
            )
            .ok_or_else(|| "Timestamp overflow".to_string())
    }
}

pub struct ContinuousQuery<'handle> {
    raw_handle: bindings::qdb_query_cont_handle_t,
    parent_handle: &'handle ClusterHandle<Connected>,
    query: Pin<CString>,
    results: Receiver<QueryResult>,
}

impl<'handle> Iterator for ContinuousQuery<'handle> {
    type Item = QueryResult;

    fn next(&mut self) -> Option<Self::Item> {
        match self.results.recv() {
            Ok(result) => Some(result),
            Err(_) => None,
        }
    }
}

extern "C" fn cont_query_callback(
    target: *mut std::ffi::c_void,
    code: bindings::qdb_error_t,
    results: *const bindings::qdb_query_result_t,
) -> std::os::raw::c_int {
    let status: Result<(), ApiError> = Status(code).into();
    if status.is_err() {
        return code;
    }

    // SAFETY
    //
    // The `target` address comes from the call in QdbHandle::query_continuous, so this
    // aforementioned function guarantees that the pointer given is coming from a
    // Arc::into_raw.
    let sender = unsafe { Arc::from_raw(target as *const Sender<QueryResult>) };
    // SAFETY
    //
    // QuasarDB guarantees us that the pointer is valid during
    // the lifetime of the continuous query callback
    let payload = match unsafe { *results }.try_into() {
        Ok(p) => p,
        Err(_) => {
            return 1;
        }
    };
    sender.send(payload).map(|_| 0).unwrap_or(1)
}

impl<'handle> Drop for ContinuousQuery<'handle> {
    fn drop(&mut self) {
        // Safety: The 'handle lifetime on a _Connected_ QdbHandle ensures that
        // the pointer to the handle is okay to use
        unsafe {
            bindings::qdb_release(
                self.parent_handle.raw_handle,
                self.raw_handle as *const std::ffi::c_void,
            )
        }
    }
}

// NOTE: this conversion trait implies an allocation to be able to expose _easily_ a safe type from the raw pointer.
// TODO: make a conversion trait that returns a reference to allow for cheap operations
impl TryFrom<&bindings::qdb_string_t> for Option<String> {
    type Error = String;

    fn try_from(value: &bindings::qdb_string_t) -> Result<Self, Self::Error> {
        if value.length == 0 {
            return Ok(None);
        }
        if value.length >= std::isize::MAX.try_into().expect("isize fits in a u64") {
            return Err(
                "The string is too long to be wrapped with the current method.".to_string(),
            );
        }
        // SAFETY
        // - The value.data pointer MUST be valid at that point
        // - value MUST represent a valid, nul-terminated C string.
        Ok(Some(unsafe {
            CStr::from_ptr(value.data).to_string_lossy().into_owned()
        }))
    }
}

/// Query result
pub struct QueryResult {
    pub header: Vec<Option<String>>,
    pub data: Vec<Vec<Option<PointResult>>>,
    pub scanned_points: usize,
    pub error_message: Option<String>,
}

impl TryFrom<bindings::qdb_query_result_t> for QueryResult {
    type Error = String;

    fn try_from(value: bindings::qdb_query_result_t) -> Result<Self, Self::Error> {
        // SAFETY
        //
        // We trust quasardb that the returned pointers are valid when we reach this point.
        // The slice is immediately converted to an owned type and data is reallocated, therefore
        // there is no issue with the lifetime of the pointer/slice
        let header: Vec<Option<String>> = unsafe {
            std::slice::from_raw_parts(
                value.column_names,
                value.column_count.try_into().expect("u64 fits in usize"),
            )
            .iter()
            .map(|col_name| col_name.try_into().unwrap())
            .collect()
        };

        let error_message: Option<String> = (&value.error_message).try_into()?;
        let scanned_points: usize = value
            .scanned_point_count
            .try_into()
            .expect("u64 fits in a usize.");

        let mut data: Vec<Vec<Option<PointResult>>> =
            Vec::with_capacity(value.row_count.try_into().expect("u64 fits in usize"));

        let sliced_data =
            unsafe { std::slice::from_raw_parts(value.rows, value.row_count.try_into().unwrap()) };
        for row in sliced_data.iter() {
            data.push(unsafe {
                std::slice::from_raw_parts(row, value.column_count.try_into().unwrap())
                    .iter()
                    .map(|point| (*point).as_ref().unwrap().try_into().unwrap())
                    .collect()
            })
        }

        Ok(Self {
            header,
            data,
            scanned_points,
            error_message,
        })
    }
}

/// Query result data
///
/// ## Multiple allocations issue
/// This structure being an owned data enum means we
/// are doing twice the memory allocations when we want to process query
/// data. It is an issue.
/// ## TODO
/// We should probably just have references for the "big" variants
/// String and Blob (to have &str and &[u8] respectively), but then there is
/// the matter of choosing a lifetime to assign to thos references.
/// Reading more of the QuasarDB documentation will give us the validity lifetime of
/// the pointers yielded by the various functions.
#[derive(Debug, Clone)]
pub enum PointResult {
    Double(f64),
    Int64(i64),
    Blob(Vec<u8>),
    Timestamp(SystemTime),
    Count(usize),
    String(String),
}

impl From<&bindings::qdb_point_result_t> for Option<PointResult> {
    fn from(res: &bindings::qdb_point_result_t) -> Self {
        // SAFETY
        //
        // The match on res.type_ asserts the type of the underlying union payload.
        // As long as Quasardb API is consistent with the tagged union this code is safe.
        match res.type_ {
            bindings::qdb_query_result_value_type_t_qdb_query_result_none => None,
            bindings::qdb_query_result_value_type_t_qdb_query_result_double => {
                Some(PointResult::Double(unsafe { res.payload.double_.value }))
            }
            bindings::qdb_query_result_value_type_t_qdb_query_result_blob => {
                Some(PointResult::Blob(unsafe {
                    std::slice::from_raw_parts(
                        res.payload.blob.content as *const u8,
                        res.payload.blob.content_length.try_into().unwrap(),
                    )
                    .to_vec()
                }))
            }
            bindings::qdb_query_result_value_type_t_qdb_query_result_int64 => {
                Some(PointResult::Int64(unsafe { res.payload.int64_.value }))
            }
            bindings::qdb_query_result_value_type_t_qdb_query_result_timestamp => {
                Some(PointResult::Timestamp(unsafe {
                    res.payload.timestamp.value.try_into().unwrap()
                }))
            }
            bindings::qdb_query_result_value_type_t_qdb_query_result_count => {
                Some(PointResult::Count(unsafe {
                    res.payload.count.value.try_into().unwrap()
                }))
            }
            bindings::qdb_query_result_value_type_t_qdb_query_result_string => {
                Some(PointResult::String(unsafe {
                    CStr::from_ptr(res.payload.string.content)
                        .to_string_lossy()
                        .into_owned()
                }))
            }
            _ => unreachable!(),
        }
    }
}

impl TryFrom<Range<SystemTime>> for bindings::qdb_ts_range_t {
    type Error = String;

    fn try_from(range: Range<SystemTime>) -> Result<Self, Self::Error> {
        Ok(Self {
            begin: range.start.try_into()?,
            end: range.end.try_into()?,
        })
    }
}

impl TryFrom<(SystemTime, Duration)> for bindings::qdb_ts_range_t {
    type Error = String;

    fn try_from((start, duration): (SystemTime, Duration)) -> Result<Self, Self::Error> {
        (start
            ..start
                .checked_add(duration)
                .ok_or_else(|| "Could not add the duration to the start date".to_string())?)
            .try_into()
    }
}
