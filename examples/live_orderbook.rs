//! Example usage to continuously monitor an orderbook

use std::borrow::Cow;

use flexi_logger::AdaptiveFormat;
use log::{info, warn};
use quasardb::{
    crypto::{self, continuous_l2_book},
    insecure_connect,
};

const DEFAULT_CLUSTER_URL: &str = "qdb://localhost:2836";

fn main() -> Result<(), anyhow::Error> {
    flexi_logger::Logger::try_with_str("trace")?
        .adaptive_format_for_stderr(AdaptiveFormat::Detailed)
        .start()?;
    let cluster_url: Cow<'static, str> = std::env::var("QDB_URI")
        .map(Cow::Owned)
        .unwrap_or(Cow::Borrowed(DEFAULT_CLUSTER_URL));
    let venue = "BitMEX";
    let instrument = "xbt_usd";
    info!("Connecting to the cluster");
    let cluster = insecure_connect(cluster_url)?;
    info!("Connected to the cluster!");
    info!("Creating tables");
    if let Err(err) = crypto::create_orderbook_bid_table(&cluster, venue, instrument) {
        warn!("Could not create the Bid table: {}", err);
    };
    if let Err(err) = crypto::create_orderbook_ask_table(&cluster, venue, instrument) {
        warn!("Could not create the Ask table: {}", err);
    };

    info!("Starting continuous query");
    let book_iter = continuous_l2_book(
        &cluster,
        venue,
        instrument,
        std::time::Duration::from_secs(10),
        Some(10),
    )?;
    for result in book_iter {
        println!("{:?}", result.header);
        for line in result.data {
            println!("{:?}", line);
        }
        println!("###########################################");
    }

    Ok(())
}
