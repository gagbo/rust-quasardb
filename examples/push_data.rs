//! Example usage to push marketdata in Quasar

use std::borrow::Cow;

use flexi_logger::AdaptiveFormat;
use log::{info, warn};
use quasardb::{
    crypto::{
        create_orderbook_ask_table, create_orderbook_bid_table, create_trades_table,
        push_ask_batch, push_bid_batch, push_trade_batch, CoinbaseEngineOrderbookRow, TradeRow,
    },
    insecure_connect,
};
use time::{Duration, OffsetDateTime};

const DEFAULT_CLUSTER_URL: &str = "qdb://localhost:2836";

fn main() -> Result<(), anyhow::Error> {
    flexi_logger::Logger::try_with_str("trace")?
        .adaptive_format_for_stderr(AdaptiveFormat::Detailed)
        .start()?;
    let cluster_url: Cow<'static, str> = std::env::var("QDB_URI")
        .map(Cow::Owned)
        .unwrap_or(Cow::Borrowed(DEFAULT_CLUSTER_URL));
    let venue = "BitMEX";
    let instrument = "xbt_usd";
    info!("Connecting to the cluster");
    let cluster = insecure_connect(cluster_url)?;
    info!("Connected to the cluster!");
    info!("Creating tables");
    if let Err(err) = create_orderbook_bid_table(&cluster, venue, instrument) {
        warn!("Could not create the Bid table: {}", err);
    };
    if let Err(err) = create_orderbook_ask_table(&cluster, venue, instrument) {
        warn!("Could not create the Ask table: {}", err);
    };
    if let Err(err) = create_trades_table(&cluster, venue, instrument) {
        warn!("Could not create the Trades table: {}", err);
    };

    push_trade_batch(
        &cluster,
        venue,
        instrument,
        vec![
            TradeRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::minutes(2))
                    .into(),
                trade_id: "How I take".to_string(),
                maker_order_id: "Makeroo".to_string(),
                taker_order_id: "taKerino".to_string(),
                sequence: 12,
                side: "Buy".to_string(),
                price: 34_000.23,
                size: 2.0,
            },
            TradeRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::minutes(1))
                    .into(),
                trade_id: "Hah I Sold".to_string(),
                maker_order_id: "Buyer".to_string(),
                taker_order_id: "Seller".to_string(),
                sequence: 13,
                side: "Sell".to_string(),
                price: 34_000.21,
                size: 1.0,
            },
            TradeRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::seconds(30))
                    .into(),
                trade_id: "Testing 0.1".to_string(),
                maker_order_id: "Buyer".to_string(),
                taker_order_id: "Seller".to_string(),
                sequence: 14,
                side: "Buy".to_string(),
                price: 35_000.67,
                size: 0.1,
            },
        ],
    )?;

    push_bid_batch(
        &cluster,
        venue,
        instrument,
        vec![
            CoinbaseEngineOrderbookRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::seconds(1328))
                    .into(),
                sequence: 1,
                price: 31_020.77,
                size: 2.0,
                type_: "Bid".to_string(),
                order_id: "Lower".to_string(),
                reason: String::new(),
                stop: String::new(),
                funds: 0.0,
                old_size: 0.0,
            },
            CoinbaseEngineOrderbookRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::seconds(1300))
                    .into(),
                sequence: 3,
                price: 31_021.77,
                size: 1.77,
                type_: "Bid".to_string(),
                order_id: "Higher".to_string(),
                reason: String::new(),
                stop: String::new(),
                funds: 0.0,
                old_size: 0.0,
            },
        ],
    )?;

    push_ask_batch(
        &cluster,
        venue,
        instrument,
        vec![
            CoinbaseEngineOrderbookRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::seconds(1327))
                    .into(),
                sequence: 2,
                price: 31_100.77,
                size: 2.0,
                type_: "Ask".to_string(),
                order_id: "Lower".to_string(),
                reason: String::new(),
                stop: String::new(),
                funds: 0.0,
                old_size: 0.0,
            },
            CoinbaseEngineOrderbookRow {
                timestamp: OffsetDateTime::now_utc()
                    .saturating_sub(Duration::seconds(1299))
                    .into(),
                sequence: 4,
                price: 31_201.77,
                size: 1.00,
                type_: "Ask".to_string(),
                order_id: "Higher".to_string(),
                reason: String::new(),
                stop: String::new(),
                funds: 0.0,
                old_size: 0.5,
            },
        ],
    )?;

    Ok(())
}
